from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_name = db.Column(db.Unicode(128), nullable=False, unique=True)
    email = db.Column(db.Unicode(128), nullable=False, unique=True)
    first_name = db.Column(db.Unicode(128))
    last_name = db.Column(db.Unicode(128))
    password = db.Column(db.Unicode(128))
    strava_token = db.Column(db.String(128))
    age = db.Column(db.String(128))
