from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, IntegerField, FloatField, SubmitField
from wtforms.validators import DataRequired, Email, Length, Regexp, EqualTo


class RegistrationForm(FlaskForm):
    user_name = StringField("User Name", validators=[DataRequired(), Regexp("[A-z0-9_]"), Length(min=4, max=20)])
    email = StringField("Email", validators=[DataRequired(), Email])
    first_name = StringField("First Name", validators=[Length(min=2, max=20), Regexp("[A-z0-9_]")])
    last_name = StringField("Last Name", validators=[Length(min=2, max=20), Regexp("[A-z0-9_]")])
    password = PasswordField("Password", validators=[DataRequired()])
    confirm_password = PasswordField("Confirm Password", validators=[DataRequired(), EqualTo(password)])
    age = IntegerField("Age", validators=[DataRequired()])
    weight = FloatField('Weight')
    max_hr = IntegerField('max_hr')
    rest_hr = IntegerField('rest_hr')
    vo2max = FloatField('vo2max')
    submit = SubmitField('Sign Up')
