from flask import Flask, render_template, request, redirect

from database import db, User
from forms.regisration_form import RegistrationForm

app = Flask(__name__)
app.config['SECRET_KEY'] = '2764b21c8e41abdcb4d0dc71e6dda844'


@app.route('/')
@app.route('/home')
def home():
    return render_template('layout.html')


@app.route('/users')
def users():
    return render_template('layout.html')


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            new_user = User()
            form.populate_obj(new_user)
            db.session.add(new_user)
            db.session.commit()
            return redirect('/users')

    return render_template("register.html", form=form)


if __name__ == '__main__':
    print(app.config)
    db.init_app(app)
    db.create_all(app=app)
    app.run(debug=True)
